
from items import ActionType, ActionCategory, ActionState

from .translator import Translator


class ActionTranslator(Translator):

    _mapTypeToCategory = {
        ActionType.run         : ActionCategory.move,
        ActionType.walk        : ActionCategory.move,
        ActionType.dance       : ActionCategory.move,
        ActionType.hide        : ActionCategory.move,
        ActionType.show        : ActionCategory.move,
        ActionType.enter       : ActionCategory.move,
        ActionType.exit        : ActionCategory.move,
        ActionType.stand       : ActionCategory.move,
        ActionType.sit         : ActionCategory.move,
        ActionType.lookAt      : ActionCategory.grimace,
        ActionType.gesticulate : ActionCategory.gesture,
        ActionType.react       : ActionCategory.gesture,
        ActionType.point       : ActionCategory.gesture,
        ActionType.pose        : ActionCategory.gesture,
        ActionType.laugh       : ActionCategory.say,
        ActionType.shout       : ActionCategory.say,
        ActionType.sing        : ActionCategory.say,
        ActionType.whisper     : ActionCategory.say,
        ActionType.talk        : ActionCategory.say,
        ActionType.give        : ActionCategory.touch,
        ActionType.take        : ActionCategory.touch,
        ActionType.throw       : ActionCategory.touch,
        ActionType.hold        : ActionCategory.touch,
        ActionType.use         : ActionCategory.touch,
        ActionType.takeOff     : ActionCategory.touch,
        ActionType.wear        : ActionCategory.touch,
        ActionType.hit         : ActionCategory.touch,
        ActionType.push        : ActionCategory.touch,
        ActionType.makeContact : ActionCategory.touch,
    }

    _mapStrToType = {
        "Run"          : ActionType.run,
        "Walk"         : ActionType.walk,
        "Hide"         : ActionType.hide,
        "Enter"        : ActionType.enter,
        "Exit"         : ActionType.exit,
        "Stand"        : ActionType.stand,
        "Sit"          : ActionType.sit,
        "Hide"         : ActionType.hide,
        "Show"         : ActionType.show,
        "Dance"        : ActionType.dance,
        "Look At"      : ActionType.lookAt,
        "Gesticulate"  : ActionType.gesticulate,
        "React"        : ActionType.react,
        "Point"        : ActionType.point,
        "Pose"         : ActionType.pose,
        "Laugh"        : ActionType.laugh,
        "Shout"        : ActionType.shout,
        "Sing"         : ActionType.sing,
        "Whisper"      : ActionType.whisper,
        "Talk"         : ActionType.talk,
        "Give"         : ActionType.give,
        "Take"         : ActionType.take,
        "Throw"        : ActionType.throw,
        "Use"          : ActionType.use,
        "Take Off"     : ActionType.takeOff,
        "Wear"         : ActionType.wear,
        "Hit"          : ActionType.hit,
        "Push"         : ActionType.push,
        "Make Contact" : ActionType.makeContact,
        "Hold"         : ActionType.hold
    }

    _mapStrToActionState = {
        "Start"   : ActionState.started,
        "Suspend" : ActionState.suspended,
        "Resume"  : ActionState.resumed,
        "Fail"    : ActionState.fail,
        "Success" : ActionState.success,
    }

    def convertStrToType(self, _type: str) -> ActionType:
        return self._convertStrToItem(_type, self._mapStrToType)

    def convertTypeToCategory(self, actionType: ActionType) -> ActionCategory:
        return self._convertStrToItem(actionType, self._mapTypeToCategory)

    def convertStrToActionState(self, state: str) -> ActionState:
        return self._convertStrToItem(state, self._mapStrToActionState)

    def _convertStrToItem(self, item: str, mapStrToItem: map) -> object:
        foundItem = None
        if item in mapStrToItem:
            foundItem = mapStrToItem[item]
        return foundItem