from items import Location, Stagecraft, ActorPosture, RelativePos


class Translator:

    _mapStrToLocation = {
        "Up Right"      : Location.upRight,
        "Up Center"     : Location.upCenter,
        "Up Left"       : Location.upLeft,
        "Middle Right"  : Location.middleRight,
        "Middle Center" : Location.middleCenter,
        "Middle Left"   : Location.middleLeft,
        "Down Right"    : Location.downRight,
        "Down Center"   : Location.downCenter,
        "Down Left"     : Location.downLeft,
        "Audience"      : Location.audience,
        "Floor"         : Location.floor,
        "Ceiling"       : Location.ceiling,
        "OffStage"      : Location.offstage
    }

    _mapStrToRelativePos = {
        "On"             : RelativePos.on,
        "In"             : RelativePos.inside,
        "Under"          : RelativePos.under,
        "Above"          : RelativePos.above,
        "Left To"        : RelativePos.leftTo,
        "Right To"       : RelativePos.rightTo,
        "Behind"         : RelativePos.behind,
        "In Front Of"    : RelativePos.inFrontOf,
        "Worn"           : RelativePos.worn,
        "Held"           : RelativePos.held,
        "Left & Behind"  : RelativePos.diagLeft1,
        "Left & Front "  : RelativePos.diagLeft2,
        "Right & Front " : RelativePos.diagRight1,
        "Right & Behind" : RelativePos.diagRight2,
    }

    _mapStrToPosture = {
        "Front"         : ActorPosture.front,
        "1/4 Right"     : ActorPosture.right1,
        "Right Profile" : ActorPosture.right2,
        "3/4 Right"     : ActorPosture.right3,
        "Back"          : ActorPosture.back,
        "1/4 Left"      : ActorPosture.left1,
        "Left Profile"  : ActorPosture.left2,
        "3/4 Left"      : ActorPosture.left3,
    }

    _mapStrToBool = {
        "Yes" : True,
        "No"  : False,
        ""    : False
    }

    _mapStrToStagecraftType = {
        "Video"    : Stagecraft.Type.video,
        "Light"    : Stagecraft.Type.light,
        "Mecanics" : Stagecraft.Type.mecanics,
        "Sound"    : Stagecraft.Type.sound,
        "Other"    : Stagecraft.Type.other,
        ""         : Stagecraft.Type.other
    }

    def convertStrToLocation(self, location: str) -> Location:
        return self._convertStrToItem(location, self._mapStrToLocation)

    def convertStrToPosture(self, posture: str) -> ActorPosture:
        return self._convertStrToItem(posture, self._mapStrToPosture)

    def convertStrToBool(self, boolean: str) -> bool:
        return self._convertStrToItem(boolean, self._mapStrToBool)

    def convertStrToRelativePos(self, position: str) -> RelativePos:
        return self._convertStrToItem(position, self._mapStrToRelativePos)

    def convertStrToStagecraftType(self, stagecraftType: str) -> Stagecraft:
        return self._convertStrToItem(stagecraftType, self._mapStrToStagecraftType)

    def _convertStrToItem(self, item: str, mapStrToItem: map) -> object:
        foundItem = None
        if item in mapStrToItem:
            foundItem = mapStrToItem[item]
        return foundItem

