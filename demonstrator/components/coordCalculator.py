import random
from tkinter.constants import HIDDEN, NORMAL

from items import Location, RelativePos, Item
from .catalogue import Catalogue


BLOCK_CENTERS = {
    Location.downRight    : (1, 4),
    Location.downCenter   : (3, 4),
    Location.downLeft     : (5, 4),
    Location.middleRight  : (1, 2),
    Location.middleCenter : (3, 2),
    Location.middleLeft   : (5, 2),
    Location.upRight      : (1, 1),
    Location.upCenter     : (3, 1),
    Location.upLeft       : (5, 1),
    Location.audience     : (3, 5),
    Location.offstage     : (12, 12),
}

HIDDEN_ELEMENTS = [
    RelativePos.under,
    RelativePos.worn,
    RelativePos.inside,
    RelativePos.held
]

NEIGHBOUR = 1/10

RELATIVE_POS = {
    RelativePos.leftTo     : (-NEIGHBOUR,0),
    RelativePos.rightTo    : (NEIGHBOUR,0),
    RelativePos.behind     : (0, -NEIGHBOUR),
    RelativePos.inFrontOf  : (0, NEIGHBOUR),
    RelativePos.diagLeft1  : (-NEIGHBOUR, -NEIGHBOUR),
    RelativePos.diagLeft2  : (-NEIGHBOUR, NEIGHBOUR),
    RelativePos.diagRight1 : (NEIGHBOUR, NEIGHBOUR),
    RelativePos.diagRight2 : (NEIGHBOUR, -NEIGHBOUR),
    RelativePos.on         : (0.01,0.01),
    RelativePos.held       : (0.08,0.08),
    RelativePos.under      : (0,0),
    RelativePos.worn       : (0,0),
    RelativePos.inside     : (0,0),
    RelativePos.above      : (0,0),
}


class CoordCalculator:

    def __init__(self, dimWindow: tuple) -> None:
        self._dimWindow = dimWindow

    def defineAllCoordAndState(self, catalogue: Catalogue) -> tuple:
        self._defineItemsCoordAndState(catalogue, catalogue.actorsDict)
        self._defineItemsCoordAndState(catalogue, catalogue.objectsDict)

    def move(self, catalogue: Catalogue, item: Item) -> tuple:
        self._defineItemCoord(catalogue, item)
        self._defineItemState(item)

    def _defineItemsCoordAndState(self, catalogue: Catalogue, itemsDict: dict) -> tuple:
        for item in itemsDict.values():
            self.move(catalogue, item)

    def _defineItemState(self, item: Item) -> None:
        location = item.location
        if type(location) is Location and item.location == Location.offstage:
            item.imageState = HIDDEN
        elif type(location) is tuple and item.location[1] in HIDDEN_ELEMENTS:
            item.imageState = HIDDEN
        else:
            item.imageState = NORMAL

    def _defineItemCoord(self, catalogue: Catalogue, item: Item) -> tuple:
        location = item.location
        if type(location) is Location and location != Location.floor and location != Location.ceiling:
            item.coordinates = self._calcultateGeneralCoord(location)
        elif type(location) is tuple:
            nearestItem = catalogue.getItemFromDict(location[0])
            if nearestItem is not None:
                if nearestItem.coordinates is None:
                    self._defineItemCoord(catalogue, nearestItem) 
                item.coordinates = self._calculateRelativeCoord(nearestItem.coordinates, location[1])
        return item.coordinates
    
    def _getItem(self, item: str, mapStrToItem: map) -> object:
        foundItem = None
        if item in mapStrToItem:
            foundItem = mapStrToItem[item]
        return foundItem

    def _printCatalogueItems(self, items: dict) -> None:
        for item in items:
            items[item].printInfos()
        print("______________")

    def _calcultateGeneralCoord(self, location: Location) -> tuple:
        deltaX = 0 #random.gauss(0, 0.3)
        deltaY = 0 #random.gauss(0, 0.3)
        blockCenterX = self._dimWindow[0]/6 * (BLOCK_CENTERS[location][0] + deltaX)
        blockCenterY = self._dimWindow[1]/6 * (BLOCK_CENTERS[location][1] + deltaY)
        return (blockCenterX, blockCenterY) 
        
    def _calculateRelativeCoord(self, itemCoord: tuple, relPosition: RelativePos = None) -> tuple :
        if relPosition is not None: 
            relPosition = RELATIVE_POS[relPosition]
            return self._calculateCoord(itemCoord, relPosition)
        else:
            #delta = (random.gauss(0, 0.1), random.gauss(0, 0.1))
            delta = (0,0)
            return self._calculateCoord(itemCoord, delta)

    def _calculateCoord(self, itemCoord: tuple, deltaTuple: tuple) -> tuple :
        blockCenterX = itemCoord[0] + self._dimWindow[0] * deltaTuple[0]
        blockCenterY = itemCoord[1] + self._dimWindow[1] * deltaTuple[1]
        return (blockCenterX, blockCenterY)