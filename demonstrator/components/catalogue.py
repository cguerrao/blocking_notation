
class Catalogue:

    def __init__(self, actors: dict, stageObjects: dict, stagecrafts: dict) -> None:
        self._actorsDict = actors
        self._objectsDict = stageObjects
        self._stagecraftsDict = stagecrafts

    @property
    def actorsDict(self) -> dict:
        return self._actorsDict

    @property
    def objectsDict(self) -> dict:
        return self._objectsDict

    def getActorsAmount(self) -> int:
        return len(self._actorsDict)

    def getItemFromDict(self, itemStr: str) -> object:
        item = self._getItem(itemStr, self._actorsDict)
        if item is None:
            item = self._getItem(itemStr, self._objectsDict)
        if item is None:
            item = self._getItem(itemStr, self._stagecraftsDict)
        return item

    def printItems(self) -> None:
        command = "h"
        while "s" not in command:
            for letter in command:
                if "a" == letter or "A" == letter:
                    self._printCatalogueItems(self._actorsDict)
                if "o" == letter or "O" == letter:
                    self._printCatalogueItems(self._objectsDict)
                if "c" == letter or "C" == letter:
                    self._printCatalogueItems(self._stagecraftsDict)
                if "h" == letter or "H" == letter:
                    print("PRESS : "
                        +"\n\t'a' to see the actors"
                        +"\n\t'o' to see the objects"
                        +"\n\t'c' to see the stagecrafts"
                        +"\n\t'h' to get help"
                        +"\n\t's' to stop"
                    )
            command = input("Which kind of items do you want to see ?\n")
    
    def _getItem(self, item: str, mapStrToItem: map) -> object:
        foundItem = None
        if item in mapStrToItem:
            foundItem = mapStrToItem[item]
        return foundItem

    def _printCatalogueItems(self, items: dict) -> None:
        for item in items:
            items[item].printInfos()
        print("______________")

