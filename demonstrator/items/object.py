from .item import Item


class StageObject(Item):

    def __init__(self,
                name: str,
                location: object,
                material: str = "", 
                colour: str = "") -> None:
        super().__init__(name, location)
        if len(colour) > 0:
            self._colour = colour
        if len(material) > 0:
            self._material = material
        else:
            self._material = "Undefined"
    
    def printInfos(self) -> None:
        print("Name: " + self._name
            + "\n\tLocation: " + str(self._location)
            + "\n\tColour: " + str(self._colour)
            + "\n\tMaterial: " + self._material
        )

    def updateLocation(self, location: object) -> None:
        self._location = location


class Costume(StageObject):

    def __init__(self,
                name: str,
                location: object,
                material: str = "",
                colour: str = "",
                cType: str = "Other") -> None:
        super().__init__(name, location, material, colour)
        if len(cType) > 0:
            self._type = cType
        else:
            self._type = "Other"

    def printInfos(self) -> None:
        super().printInfos()
        print("\tCategory: Costume\n\tType: " + str(self._type))


class Set(StageObject):

    def printInfos(self) -> None:
        super().printInfos()
        print("\tCategory: Set")


class Prop(StageObject):

    def printInfos(self) -> None:
        super().printInfos()
        print("\tCategory: Prop")
