from enum import IntEnum

from .item import Item
from .actionStatus import ActionType, ActionState, ActionCategory
from .action import Action

TIME_IN_MS = 2000

class ActorPosture(IntEnum):
    front = 0
    right1 = 1
    right2 = 2
    right3 = 3
    back = 4
    left1 = 5
    left2 = 6
    left3 = 7

class Actor(Item):

    def __init__(self,
                name: str,
                location: object,
                state: list = [0,0,0,0,0,0,0],
                posture: ActorPosture = ActorPosture.front) -> None:
        super().__init__(name, location)
        self._state = state
        self._posture = posture
        self._heldObjects = []

    def printInfos(self) -> None:
        print("Name: " + self._name
            + "\n\tLocation: " + str(self._location)
            + "\n\tState: " + str(self._state)
            + "\n\tPosture: " + str(self._posture)
            )

    def printState(self) -> None:
        print("Name: " + self._name
            + "\n\tVisible: " + str(self._state[0])
            + "\n\tMoving: " + str(self._state[1])
            + "\n\tSitted: " + str(self._state[2])
            + "\n\tSpeaking: " + str(self._state[3])
            + "\n\tHolding: " + str(self._state[4])
            + "\n\tTouching: " + str(self._state[5])
            + "\n\tNonVerbal: " + str(self._state[6]) 
            )

    def pose(self, posture: ActorPosture) -> None:
        self._posture = posture

    def actMove(self,
            time: int,
            actionType: ActionType,
            actionState: ActionState,
            params: list) -> None:
        actionsList = self._graph[ActionCategory.move]
        self._act(time, actionType, actionState, params, actionsList)

    def actSay(self,
            time: int,
            actionType: ActionType,
            actionState: ActionState,
            params: list) -> None:
        actionsList = self._graph[ActionCategory.say]
        self._act(time, actionType, actionState, params, actionsList)
        raise NotImplementedError

    def actTouch(self,
            time: int,
            actionType: ActionType,
            actionState: ActionState,
            params: list) -> None:
        actionsList = self._graph[ActionCategory.touch]
        self._act(time, actionType, actionState, params, actionsList)
        raise NotImplementedError

    def actGesture(self,
            time: int,
            actionType: ActionType,
            actionState: ActionState,
            params: list) -> None:
        actionsList = self._graph[ActionCategory.gesture]
        self._act(time, actionType, actionState, params, actionsList)
        raise NotImplementedError

    def actGrimace(self,
            time: int,
            actionType: ActionType,
            actionState: ActionState,
            params: list) -> None:
        actionsList = self._graph[ActionCategory.grimace]
        self._act(time, actionType, actionState, params, actionsList)
        raise NotImplementedError

    def _act(self,
            time: int,
            actionType: ActionType,
            actionState: ActionState,
            params: list,
            actionsList: list) -> None:
        if len(actionsList) > 0:
            lastAction = actionsList[-1]
            if lastAction.compare(actionType, params):
                if actionState in (ActionState.suspended, ActionState.success, ActionState.fail):
                    lastAction.finalize(time+TIME_IN_MS, actionState)
                elif actionState == ActionState.resumed and lastAction.status[0] == ActionState.suspended:
                    lastAction.finalize(time+TIME_IN_MS, actionState)
                    self._addActionToGraph(time, actionType, actionState, params, actionsList)
            else:
                lastAction.finalize(time, ActionState.success)
                self._addActionToGraph(time, actionType, actionState, params, actionsList)
        else:
            self._addActionToGraph(time, actionType, actionState, params, actionsList)

    def _addActionToGraph(self,
            time: int,
            actionType: ActionType,
            actionState: ActionState,
            params: list,
            actionsList: list) -> None:
            if actionState in (ActionState.resumed, ActionState.started):
                actionsList.append(Action(self._coordinates, time, actionType, params))
            elif actionState in (ActionState.success, ActionState.fail):
                action = Action(self._coordinates, time, actionType, params)
                action.finalize(time+TIME_IN_MS, actionState)
                actionsList.append(action)

    def printOneCategoryGraph(self, category: ActionCategory) -> None:
        for action in self._graph[category]:
            print(str(action))