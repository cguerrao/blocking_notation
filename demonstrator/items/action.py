from .actionStatus import ActionState, ActionType

class Action:

    def __init__(self,
                itemCoord: tuple,
                timeBegin: int,
                actionType: ActionType,
                params: list) -> None:
        self._itemCoord = itemCoord
        self._type = actionType
        self._time = [timeBegin, None]
        self._finalized = False
        self._success: ActionState = None
        self._params = params
    
    def __str__(self) -> str:
        return ( "\nType    :" + str(self._type)
               + "\nSuccess :" + str(self._success)
               + "\nParams  :" + str(self._params)
               + "\nTime    :" + str(self._time)
               + "\nCood    :" + str(self._itemCoord))

    @property
    def coord(self) -> tuple:
        return self._itemCoord

    @property
    def time(self) -> list:
        return self._time

    @property
    def status(self) -> tuple:
        return (self._type, self._success)
        
    @property
    def finalized(self) -> None:
        return self._finalized

    def compare(self, _type: ActionType, params: list) -> bool:
        if self._type != _type or len(params) != len(self._params):
            return False
        for param in params:
            if param not in self._params:
                return False
        return True

    def finalize(self, timeEnd: int, actionState: ActionState) -> None:
        if not(self._finalized):
            if actionState in (ActionState.started, ActionState.resumed):
                raise ValueError("An action doesn't end with the state: "+ actionState)
            self._time[1]= timeEnd
            self._success = actionState
            self._finalized = True

