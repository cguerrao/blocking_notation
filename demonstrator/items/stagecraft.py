from enum import IntEnum

from .item import Item
from .location import Location

class Stagecraft(Item):

    class Type(IntEnum):
        sound = 0
        light = 1
        video = 2
        mecanics = 3
        other = 4
    
    def __init__(self,
                craftName: str,
                craftType: Type = Type.other,
                description: str = "Undefined",
                activated: bool = False,
                intensity: int = 0) -> None:
        super().__init__(craftName, Location.floor)
        self._currentIntensity = intensity
        self._on = activated
        self._type = craftType
        self._description = description

    def printInfos(self) -> None:
        print("Id: " + self._name
            + "\n\tOn: " + str(self._on)
            + "\n\tIntensity: " + str(self._currentIntensity)
            + "\n\tType: " + str(self._type)
            + "\n\tDescription: " + self._description
            )