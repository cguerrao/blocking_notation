from enum import IntEnum


class Location(IntEnum):
    upRight = 0
    upCenter = 1
    upLeft = 2
    middleRight = 3
    middleCenter = 4
    middleLeft = 5
    downRight = 6
    downCenter = 7
    downLeft = 8
    offstage = 9
    floor = 10
    ceiling = 11
    audience = 12


class RelativePos(IntEnum):
    on = 0
    inside = 1
    under = 2
    above = 3
    leftTo = 4
    rightTo = 5
    inFrontOf = 6
    behind = 7
    worn = 8
    held = 9
    diagLeft1 = 10
    diagLeft2 = 11
    diagRight1 = 12
    diagRight2 = 13