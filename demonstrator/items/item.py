from tkinter.constants import NORMAL

from .actionStatus import ActionCategory

class Item:

    def __init__(self, name: str, location: object) -> None:
        self._coordinates = None
        self._location = location
        self._name = name
        self._imageState = NORMAL
        self._colour = "white"
        self._graph = {
            ActionCategory.move    : [],
            ActionCategory.say     : [],
            ActionCategory.touch   : [],
            ActionCategory.gesture : [],
            ActionCategory.grimace : []
        }

    def __str__(self) -> str:
        return self._name

    @property
    def coordinates(self) -> tuple:
        return self._coordinates

    @coordinates.setter
    def coordinates(self, newCoord: tuple) -> None:
        self._coordinates = newCoord

    @property
    def colour(self) -> str:
        return self._colour

    @property
    def imageState(self) -> object:
        return self._imageState

    @imageState.setter
    def imageState(self, newState: object) -> None:
        self._imageState = newState

    @property
    def location(self) -> str:
        return self._location
    
    def move(self, location: object) -> None:
        self._location = location
            
    @property
    def graph(self) -> map:
        return self._graph