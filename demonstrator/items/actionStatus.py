from enum import IntEnum

class ActionState(IntEnum):
    started = 0
    suspended = 1
    resumed = 2
    success = 3
    fail = 4

class ActionCategory(IntEnum):
    move = 0
    say = 1
    touch = 2
    gesture = 3
    grimace = 4

class ActionType(IntEnum):
    enter = 0
    exit = 1
    stand = 2
    sit = 3
    hide = 4
    show = 5
    walk = 6
    run = 7
    dance = 8
    laugh = 9
    shout = 10
    sing = 11
    whisper = 12
    talk = 13
    gesticulate = 14
    react = 15
    pose = 16
    point = 17
    lookAt = 18
    give = 19
    take = 20
    throw = 21
    use = 22
    takeOff = 23
    wear = 24
    hit = 25
    push = 26
    makeContact = 27
    hold = 28

