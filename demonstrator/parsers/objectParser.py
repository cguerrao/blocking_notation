import pandas as pd

from .parser import Parser
from items import Set, Costume, Prop


class ObjectParser(Parser):

    def initialiseStageObjects(self) -> dict:
        stageObjects = {}
        self._addCostumesToDict(stageObjects)
        self._addPropsToDict(stageObjects)
        self._addSetsToDict(stageObjects)
        return stageObjects

    def _addCostumesToDict(self, stageObjects: dict) -> None:
        notations = self._readExcel(Parser.SheetName.costume)
        for index in range(1, len(notations)):
            name, material, colour, position = self._readObjectData(notations, index, 2)
            type = str(notations.iloc[index, 4])
            if len(name) > 0:
                stageObjects[name] = Costume(name, position, material, colour, type)

    def _addPropsToDict(self, stageObjects: dict) -> None:
        notations = self._readExcel(Parser.SheetName.prop)
        for index in range(1, len(notations)):
            name, material, colour, position = self._readObjectData(notations, index)
            if len(name) > 0:
                stageObjects[name] = Prop(name, position, material, colour)

    def _addSetsToDict(self, stageObjects: dict) -> None:
        notations = self._readExcel(Parser.SheetName.set)
        for index in range(1, len(notations)):
            name, material, colour, position = self._readObjectData(notations, index)
            if len(name) > 0:
                stageObjects[name] = Set(name, position, material, colour)

    def _readObjectData(self, notations: pd.DataFrame, index: int, offset: int = 0) -> None:
        name = str(notations.iloc[index, 0])
        material = str(notations.iloc[index, 1])
        colour = str(notations.iloc[index, 2])
        position = self._findPosition(notations, index, offset+3)
        return name, material, colour, position

