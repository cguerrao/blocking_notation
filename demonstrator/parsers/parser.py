from enum import IntEnum
import pandas as pd

from components import Translator
from items import Location


class Parser():

    class SheetName(IntEnum):
        actor = 0
        costume = 1
        prop = 2
        set = 3
        stagecraft = 4
        net = 5

    _sheetNames = {
        SheetName.actor      : "Actor",
        SheetName.costume    : "Costume",
        SheetName.prop       : "Prop",
        SheetName.set        : "Set",
        SheetName.stagecraft : "Stagecraft",
        SheetName.net        : "Step by step"
    }

    def __init__(self, path: str) -> None:
        self._path = path

    def _readExcel(self, sheetName: SheetName) -> None:
        sheetNameStr = self._sheetNames[sheetName]
        return pd.read_excel(self._path, sheet_name=sheetNameStr, keep_default_na=False)

    def _findPosition(self, notations: pd.DataFrame, indexRow: int, indexColumn: int) -> object:
        read = str(notations.iloc[indexRow, indexColumn])
        position = None
        if len(read) == 0:
            position = Location.offstage
        else:
            translator = Translator()
            position = translator.convertStrToLocation(read)
            if position is None:
                relativePosition = translator.convertStrToRelativePos(notations.iloc[indexRow, indexColumn+1])
                position = (str(read), relativePosition)
        return position

