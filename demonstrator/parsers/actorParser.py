import pandas as pd

from .parser import Parser
from items import Actor, Location
from components import Translator


class ActorParser(Parser):

    def initialiseActors(self) -> dict:
        notations = self._readExcel(Parser.SheetName.actor)
        actors = {}
        self._addActorsToDict(actors, notations)
        return actors

    def _addActorsToDict(self, actors: dict, notations: pd.DataFrame) -> None:
        for index in range(1, len(notations)):
            name = str(notations.iloc[index, 0])
            if len(name) > 0:
                position = self._findPosition(notations, index, 1)
                posture = Translator().convertStrToPosture(notations.iloc[index, 3])
                state = self._getActorState(notations, index)
                if position == Location.offstage:
                    actors[name] = Actor(name, Location.offstage)
                else:
                    state = self._getActorState(notations, index)
                    actors[name] = Actor(name, position, state, posture)

    def _getActorState(self, notations: pd.DataFrame, index: int) -> list:
        translator = Translator()
        visible = translator.convertStrToBool(notations.iloc[index, 4])
        moving = translator.convertStrToBool(notations.iloc[index, 5])
        sitted = translator.convertStrToBool(notations.iloc[index, 6])
        speaking = translator.convertStrToBool(notations.iloc[index, 7])
        holding = translator.convertStrToBool(notations.iloc[index, 8])
        touching = translator.convertStrToBool(notations.iloc[index, 9])
        nonverbal = translator.convertStrToBool(notations.iloc[index, 10])

        return [visible, moving, sitted, speaking, holding, touching, nonverbal]

