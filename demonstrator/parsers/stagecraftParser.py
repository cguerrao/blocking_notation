import pandas as pd

from .parser import Parser
from items import Stagecraft
from components import Translator


class StagecraftParser(Parser):

    def initialiseStagecrafts(self) -> dict:
        notations = self._readExcel(Parser.SheetName.stagecraft)
        stagecrafts = {}
        self._addStagecraftsToDict(stagecrafts, notations)
        return stagecrafts

    def _addStagecraftsToDict(self, stagecrafts: dict, notations: pd.DataFrame) -> None:
        translator = Translator()
        for index in range(1, len(notations)):
            name = str(notations.iloc[index, 0])
            if len(name) > 0:
                cueType, description, activated, intensity = self._readStagecraftValues(notations, translator, index)
                stagecrafts[name] = Stagecraft(name, cueType, description, activated, intensity)

    def _readStagecraftValues(self, notations: pd.DataFrame, translator: Translator, index: int) -> object:
        cueType = translator.convertStrToStagecraftType(notations.iloc[index, 1])
        description = str(notations.iloc[index, 2])
        activated = translator.convertStrToBool(notations.iloc[index, 3])
        intensity = int(notations.iloc[index, 4])
        return cueType, description, activated, intensity

