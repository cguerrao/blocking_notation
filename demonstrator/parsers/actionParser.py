import pandas as pd

from components import Translator, Catalogue, CoordCalculator
from items import Location, TIME_IN_MS, Actor, ActionCategory
from .parser import Parser

ACTOR_NB_COLUMNS = 16

class ActionParser(Parser):

    def __init__(self, path: str, catalogue: Catalogue, windowDim: int) -> None:
        super().__init__(path)
        self._notations = self._readExcel(Parser.SheetName.net)
        self._catalogue = catalogue
        self._translator = Translator()
        self._calculator = CoordCalculator(windowDim)

    def read(self) -> None:
        nbColumnsForActor = self._catalogue.getActorsAmount() * (ACTOR_NB_COLUMNS+1)
        currentTime = -TIME_IN_MS
        for rowIndex in range(2, len(self._notations)):
            currentTime = currentTime + TIME_IN_MS
            for columnIndex in range(5, nbColumnsForActor, ACTOR_NB_COLUMNS+1):
                actorName = self._notations.columns[columnIndex]
                actor = self._catalogue.getItemFromDict(actorName)
                self._readMoveAction(actor, currentTime, rowIndex, columnIndex)
                '''self._readSayAction(actor, currentTime, rowIndex, columnIndex)
                self._readTouchAction(actor, currentTime, rowIndex, columnIndex)
                self._readGestureAction(actor, currentTime, rowIndex, columnIndex)
                self._readGrimaceAction(actor, currentTime, rowIndex, columnIndex)'''

    def _readMoveAction(self, actor: Actor, time: int, rowIndex: int, columnIndex: int) -> None:
        state, _type, param1 = self._read3AttributesAction(rowIndex, columnIndex)
        if _type is not None:
            location = self._converParamToLocationFormat(param1)
            if location is not None:
                actor.move(location)
                self._calculator.move(self._catalogue, actor)
            actor.actMove(time, _type, state, param1)

    def _readSayAction(self, actor: Actor, time: int, rowIndex: int, columnIndex: int) -> None:
        state, _type, listeners = self._read3AttributesAction(rowIndex, columnIndex+3)
        if _type is not None:
            actor.actSay(time, _type, state, listeners)

    def _readTouchAction(self, actor: Actor, time: int, rowIndex: int, columnIndex: int) -> None:
        state, _type, param1, param2 = self._read4AttributesAction(rowIndex, columnIndex+6)
        if _type is not None:
            actor.actTouch(time, _type, state, param1, param2)
    
    def _readGestureAction(self, actor: Actor, time: int, rowIndex: int, columnIndex: int) -> None:
        state, _type, param1 = self._read3AttributesAction(rowIndex, columnIndex+10)
        if _type is not None:
            actor.actGesture(time, _type, state, param1)
    
    def _readGrimaceAction(self, actor: Actor, time: int, rowIndex: int, columnIndex: int) -> None:
        state, _type, param1 = self._read3AttributesAction(rowIndex, columnIndex+13)
        if _type is not None:
            actor.actGrimace(time, _type, state, param1)

    def _converParamToLocationFormat(self, param: list) -> None:
        if len(param) == 2: # Case: Relative Location (Item , RelativePosition)
            return (str(param[0]), param[1])
        elif len(param) == 1 and type(param[0]) is Location: # Case : General Location
            return param[0]
        elif len(param) == 1 and param[0] is None: # Case : Empty cell
            return None
        elif len(param) == 1: # Case : Relative Location without any mention of the relative position
            return (str(param[0]), None)

    def _read3AttributesAction(self, rowIndex: int, columnIndex:int) -> None:
        state, type, param1, param2 = self._readOneAction(rowIndex, columnIndex)
        del param2
        return state, type, param1

    def _read4AttributesAction(self, rowIndex: int, columnIndex:int) -> None:
        state, type, param1, param2 = self._readOneAction(rowIndex, columnIndex)
        return state, type, [param1, param2]

    def _readOneAction(self, rowIndex: int, columnIndex :int) -> object:
        state = self._translator.convertStrToActionState(self._notations.iloc[rowIndex, columnIndex])
        type = self._translator.convertStrToType(self._notations.iloc[rowIndex, columnIndex+1])
        param1 = self._readParams(self._notations.iloc[rowIndex, columnIndex+2])
        param2 = self._readParams(self._notations.iloc[rowIndex, columnIndex+3])
        return state, type, param1, param2

    def _readParams(self, paramCell: str) -> list:
        paramsList = paramCell.split(";")
        itemsList = []
        for param in paramsList:
            itemsList.append(self._getItem(param))
        return itemsList

    def _getItem(self, item: str) -> object:
        found = None
        if len(item) > 0:
            found = self._catalogue.getItemFromDict(item)
            if found is None:
                found = self._translator.convertStrToLocation(item)
            if found is None:
                found = self._translator.convertStrToPosture(item)
            if found is None:
                found = self._translator.convertStrToRelativePos(item)
            if found is None:
                found = item
        return found
