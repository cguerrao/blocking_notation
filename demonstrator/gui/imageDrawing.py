from tkinter import Canvas, PhotoImage
from tkinter.constants import HIDDEN, NORMAL, NW
from PIL import Image, ImageTk

from items import Item
from .drawing import Drawing

class ImageDrawing(Drawing):

    def __init__(self, canvas: Canvas, path: str, item: Item) -> None:
        super().__init__(canvas, item.coordinates)
        self._image= ImageTk.PhotoImage(Image.open(path))
        self._imageId = canvas.create_image(self._coord[0],
                                            self._coord[1],
                                            anchor=NW,
                                            image=self._image,
                                            state=item.imageState)
    @property    
    def image(self) -> PhotoImage:
        return self._image

    def _move(self, newCoord: tuple) -> None:
        self._canvas.moveto(self._imageId, newCoord[0], newCoord[1])

    def _hide(self) -> None:
        self._canvas.itemconfigure(self._imageId, state=HIDDEN)

    def _show(self) -> None:
        self._canvas.itemconfigure(self._imageId, state=NORMAL)

