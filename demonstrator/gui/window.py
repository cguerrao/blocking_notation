from tkinter import Tk, Label

from components import Catalogue

from .video import Video
from .sketch import Sketch

BACKGROUND_COLOR = "black"
TEXT_COLOR = "white"
MARGIN = 5

class Window:

    def __init__(self, fileName: str):
        self._root = Tk()
        self._setRootConfig()
        self._setTitle(fileName)

        self._video = Video(fileName, self._root, MARGIN)
        self._sketch = Sketch(fileName, self._root, MARGIN)

    def getSketchDimensions(self) -> tuple:
        return self._sketch.getDimensions()

    def drawInitPlay(self, catalogue: Catalogue) -> None:
        self._sketch.drawInitPlay(catalogue.actorsDict, catalogue.objectsDict)

    def drawGraph(self, catalogue: Catalogue) -> None:
        self._sketch.drawGraph(catalogue.actorsDict, catalogue.objectsDict)

    def _setRootConfig(self) -> None:
        self._root.title("Blocking Notation")
        self._root.geometry(str(self._root.winfo_screenwidth())+"x"+str(self._root.winfo_screenheight()))
        self._root.minsize(1280, 720)
        self._root.config(background=BACKGROUND_COLOR)

    def _setTitle(self, fileName) -> None:
        title = Label(self._root,
                        text="The Blocking Notation - "+fileName,
                        font=("Courrier", 40),
                        bg=self._root["bg"],
                        fg=TEXT_COLOR)
        title.pack(pady=MARGIN)

    def play(self) -> None:
        self._root.mainloop()

