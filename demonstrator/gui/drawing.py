from tkinter import Canvas

from items import ActionType

MOVE = [
    ActionType.walk,
    ActionType.run,
    ActionType.dance
]

IDLE_COLOUR = "white"

class Drawing:

    def __init__(self, canvas: Canvas, coordinates: tuple) -> None:
        self._canvas = canvas
        self._coord = coordinates

    def act(self, status: tuple, coord: tuple, time: list) -> None:
        self._canvas.after(time[1], lambda: self._move(coord))
        '''if status[0] in MOVE:
            self._canvas.after(time[1], lambda: self._move(coord))
        elif status[0] in (ActionType.hide, ActionType.exit):
            self._canvas.after(time[0], lambda: self._hide())
        elif status[0] in (ActionType.show, ActionType.enter):
            self._canvas.after(time[0], lambda: self._show())
        elif status[0] == ActionType.sit:
            self._canvas.after(time[0], lambda: self._sit(coord))
        elif status[0] == ActionType.stand:
            self._canvas.after(time[0], lambda: self._stand(coord))'''
        #self._canvas.after(time[1], lambda: self._showState(ActionCategory.move, status[1]))

    def _move(self, newCoord: tuple) -> None:
        del newCoord
        raise NotImplementedError

    def _hide(self) -> None:
        raise NotImplementedError

    def _show(self) -> None:
        raise NotImplementedError

    def _sit(self, newCoord: tuple) -> None:
        del newCoord
        raise NotImplementedError

    def _stand(self, newCoord: tuple) -> None:
        del newCoord
        raise NotImplementedError

