from math import ceil
from tkinter import *
from PIL import Image, ImageTk
from pathlib import Path
import imageio
import os

PERSONAL_PATH = os.getcwd()+"/"
VIDEO_PATH = "demonstrator/utils/videos/"
VIDEO_FORMAT = ".mp4"

TEXT_COLOR = "black"
BTEXT_START = "Restart"
BTEXT_STOP  = "Stop"

class Video:

    def __init__(self, fileName: str, window: Tk, margin: int):
        videoName = PERSONAL_PATH+VIDEO_PATH+fileName+VIDEO_FORMAT
        self._setComponents(window, margin)
        if Path(videoName).is_file():
            self._video = imageio.get_reader(videoName)
            self._stream()
        else:
            warningMessage = ("\nWARNING: there is no video "+fileName+VIDEO_FORMAT
                            +"\nAdd one in the folder demonstrator/utils/videos")
            
            warningLabel = Label(self._videoLabel,
                        text=warningMessage,
                        font=("Courrier", 20),
                        bg=window["bg"],
                        fg="white")
            warningLabel.pack(side=TOP)

            print(warningMessage)

    def _setComponents(self, window: Tk, margin: int) -> None:
        videoFrame = Frame(bg=window["bg"],
                            height=int(window.winfo_screenheight()),
                            width=int(window.winfo_screenwidth()/2-50))
        self._videoLabel = Label(videoFrame,
                                bg=videoFrame["bg"],
                                height=int(window.winfo_screenheight()),
                                width=int(window.winfo_screenwidth()/2-50))

        self._videoLabel.pack(side=LEFT,padx=margin, pady=margin)
        videoFrame.pack(side=LEFT,padx=margin, pady=margin)

    def _launchPlay(self) -> None:
        self._videoDisplaying = not(self._videoDisplaying)
        if self._videoDisplaying:
            self._playButton.config(text=BTEXT_STOP)
        else:
            self._playButton.config(text=BTEXT_START)
        self._stream()

    def _calculateRatio(self) -> int:
        size = self._video.get_meta_data()["size"]
        self._videoLabel.update()
        if size[0] >= size[1]:
            ratio = ceil(self._videoLabel.winfo_screenwidth()/size[0])
        else:
            ratio = ceil(self._videoLabel.winfo_screenheight()/size[1])
        return ratio

    def _stream(self):
        try:
            image = self._video.get_next_data()
            ratio = self._calculateRatio()
            frame_image = Image.fromarray(image).reduce(ratio)
            frame_image = ImageTk.PhotoImage(frame_image)
            self._videoLabel.config(image=frame_image)
            self._videoLabel.image = frame_image
            self._videoLabel.after(20, lambda: self._stream())
        except:
            self._video.close()
            return

