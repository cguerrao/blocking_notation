from tkinter import Canvas, Tk
from tkinter.constants import RIGHT, NW
from PIL import Image, ImageTk
from pathlib import Path

from items import Prop, Costume, Item, ActionCategory
from .iconDrawing import IconDrawing
from .imageDrawing import ImageDrawing

PICTURE_PATH = "demonstrator/utils/pictures/"
PICTURE_FORMAT = ".png"

class Sketch:

    def __init__(self, fileName: str, root: Tk, margin: int) -> None:
        self._stageCanvas = Canvas(root, bg="white", height=root.winfo_screenheight(), width=root.winfo_screenwidth()/2)
        self._stageCanvas.pack(side=RIGHT,padx=margin, pady=margin)

        self._dimensions = (root.winfo_screenwidth()/2, root.winfo_screenheight())
        self._imagesList = []
        self._mapObjectToDrawing = {}
        self._fileName = fileName
    
    def getDimensions(self) -> tuple:
        return self._dimensions
        
    def drawInitPlay(self, actorDict: dict, objectDict: dict) -> None:
        pathName = PICTURE_PATH+self._fileName+PICTURE_FORMAT
        if Path(pathName).is_file():
            img= ImageTk.PhotoImage(Image.open(pathName))  
            self._stageCanvas.create_image(0, 0, anchor=NW, image=img)
            self._imagesList.append(img)
        else:
            print("\nWARNING: there is no picture "+self._fileName+PICTURE_FORMAT
                    +" representing the set. Add one in the folder demonstrator/utils/pictures")
        
        self._drawInitObjects(objectDict)
        self._drawInitActors(actorDict)
        self._stageCanvas.image_names = self._imagesList

    def _drawInitActors(self, actorDict: dict) -> None:
        for actorName in actorDict:
            actor = actorDict[actorName]
            self._createDrawing(actorName, actor)

    def _drawInitObjects(self, objectDict: dict) -> None:
        for objectName in objectDict:
            stageObject = objectDict[objectName]
            if type(stageObject) is Prop or type(stageObject) is Costume:
                self._createDrawing(objectName, stageObject) 

    def _createDrawing(self, name: str, item: Item) -> None:
        pathName = PICTURE_PATH+name+PICTURE_FORMAT
        if Path(pathName).is_file():
            drawing = ImageDrawing(self._stageCanvas, pathName, item)
            self._mapObjectToDrawing[name] = drawing 
            self._imagesList.append(drawing.image)
        else:
            drawing = IconDrawing(self._stageCanvas, item)
            self._mapObjectToDrawing[name] = drawing        

    def drawGraph(self, actorsDict: dict, objectsDict: dict) -> None:
        for actorName in actorsDict:
            actor = actorsDict[actorName]
            for action in actor.graph[ActionCategory.move]:
                self._mapObjectToDrawing[actorName].act(action.status, action.coord, action.time)

