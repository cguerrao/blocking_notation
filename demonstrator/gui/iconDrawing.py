from tkinter import Canvas
from tkinter.constants import HIDDEN, NORMAL
import math

from PIL.Image import new

from items import Item
from .drawing import Drawing

SIZE = 50

class IconDrawing(Drawing):

    def __init__(self, canvas: Canvas, item: Item) -> None:
        super().__init__(canvas, item.coordinates)
        self._rectangle = canvas.create_rectangle(self._coord[0],
                                                    self._coord[1],
                                                    self._coord[0]+SIZE,
                                                    self._coord[1]+SIZE,
                                                    fill=item.colour,
                                                    state=item.imageState)
        self._text = canvas.create_text(self._coord[0]+SIZE/2,
                                        self._coord[1]+SIZE/2,
                                        text=str(item),
                                        state=item.imageState)

    def _move(self, newCoord: tuple) -> None:
        self._canvas.moveto(self._rectangle, newCoord[0], newCoord[1])
        self._canvas.moveto(self._text, newCoord[0], newCoord[1])

    def _hide(self) -> None:
        self._canvas.itemconfigure(self._rectangle, state=HIDDEN)
        self._canvas.itemconfigure(self._text, state=HIDDEN)

    def _show(self) -> None:
        self._canvas.itemconfigure(self._rectangle, state=NORMAL)
        self._canvas.itemconfigure(self._text, state=NORMAL)

