from pathlib import Path

from gui import Window
from parsers import ActorParser, ObjectParser, StagecraftParser, ActionParser
from components import Catalogue, CoordCalculator


EXCEL_PATH = "demonstrator/utils/notations/"
EXCEL_FORMAT = ".xlsx"

def _initCatalogue(excelPath):
    actors = ActorParser(excelPath).initialiseActors()
    stageObjects = ObjectParser(excelPath).initialiseStageObjects()
    stagecrafts = StagecraftParser(excelPath).initialiseStagecrafts()
    return Catalogue(actors, stageObjects, stagecrafts)

def run():
    fileName = input("\nEnter Excel file's name: ")
    excelPath = EXCEL_PATH + fileName + EXCEL_FORMAT
    while not(Path(excelPath).is_file()):
        fileName = input("\nFile not found in the directory "+EXCEL_PATH+".\nEnter new Excel file's name: ")
        excelPath = EXCEL_PATH + fileName + EXCEL_FORMAT
    
    catalogue = _initCatalogue(excelPath)
    window = Window(fileName)
    windowDim = window.getSketchDimensions()

    # Initialise items' coordinates
    calculator = CoordCalculator(windowDim)
    calculator.defineAllCoordAndState(catalogue)

    # Initialise items on the window
    window.drawInitPlay(catalogue)
    
    # Construct items' graphs 
    actionParser = ActionParser(excelPath, catalogue, windowDim)
    actionParser.read()

    window.drawGraph(catalogue)
    window.play()


if __name__== "__main__":
    run()