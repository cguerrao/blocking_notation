# Blocking Notation

**Author :** Camélia Guerraoui

The purpose of the *Blocking Notation* project is to develop a formal representation of a theater play. 

This representation relies on :
- an ontology for theater performances
- a graph of actions performed on stage

The *Blocking Notation* project is composed of multiple folders:

1. **ontology** : contains an ontology called *Blocking Notation*. 

2. **excel notations** : contains files to notate video fo play

3. **demonstrator** : a tool to visualise the excel notations

4. **article** : contains the research article

## Ontology 
This ontology helps to describe a theater performance . This includes:
  - the actors, characters
  - visual actions performed on stage.
  - sets, costumes, props
  - technical elements performed during the play (such as light, video, sound).
The proposed actions are only factual, i.e. are independent of any subtexts, emotions.

## Excel notations
This folder contains a template to write *Blocking Notation*, its documentation and previous notations.

## Demonstrator

### POC
This interface allows to visualise Move Action. We leave for future work the visualisation of other action categories.

### Requirements
To launch the graphical interface to visualise the *Blocking Notation*, you need first too install:
- Python
- the Python library TKinter
- the Python library Pandas

### Set up
Before launching the interface : 
- Add the Excel notations file in the folder demonstrator > utils > notations.
- Add the corresponding video file in the folder demonstrator > utils > videos. The video must have the exact same name as the Excel notations and be a MP4 video. If there isn't any video, a warning will be displayed in the terminal but the application will be launched.
- Add the corresponding background picture representing the set in the folder demonstrator > utils > pictures. This picture have the exact same name as the Excel notations and have a PNG Extension. If there isn't any picture, a warning will be displayed in the terminal but the application will be launched.
- (Optional) Add pictures of the actors, props, costumes and set elements which you defined in the Excel notations file in the folder demonstrator > utils > pictures. The pictures must have the exact same name as the name defined in the Excel notation file. It is better to use small pictures (around 50x50 pixel). The picture must be a PNG image. If an element doesn't have a corresponding picture, it will appear as a rectangle with the colour defined in the Excel file. If there is not any defined colour for this item, the rectangle will appear white. To define a specific colour, you can check the Colour Chart (demonstrator > utils > notations > Color_Chart.png).

### Command to launch the interface
From a terminal : 

python demonstrator/play.py

Then the interface will ask which excel file you want to visualise, write the file name without its extension.

## Article

This folder contains the Latex Code to generate the Blocking Notation Article and the corresponding output.