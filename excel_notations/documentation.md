# Excel Template - Documentation

## How the Excel template works
To annotate a video of a theater performance with our Excel Template, you have to define first the actors, costumes, props and set elements that you need for the whole video in the corresponding sheets. After these definitions, you can start writing the notations in the "Step by step" sheet.

Check the blocking_notation.owl file in the ontology directory to see the signification of each Excel element and how each action should be notated.

The "Statistics" sheet is a brief dashboard for your notations. It will be automatically be updated when a notation is written.

Our template automatically checks if some spelling mistakes were made. If any mistakes were made, the cell's background will become red. If you want to add new specific terms, you will have to add them in the "Vocabulary" sheet.

## Conventions to write correct Blocking Notations

### 1. Fast action
If a continuous action happens very fast (i.e. in 2 seconds or less), only the notation to indicate the end of the action should be written.

### 2. A Group as Parameter
If several parameters have to be marked, they should be defined as a group i.e. every parameter are written in the same cell and are separated with a semicolon without any extra space. 

Be aware that our template cannot for the moment check spelling mistakes of group notation. When a group is written in a cell, this cell will appear red.

### 3. Stage location
The stage is divided into nine blocks. If a stage object or an actor is located on a border between two blocks, the block which is the closest to the audience and the most in the center, should be written.

### 4. Left and Right
To describe a stageObject's or an actor's position, prepositions of place such as "Left To" and "Right To" can be used. These orientations are marked with the audience's point of view (and not from the actor's). But all nine locations on stage are from the perspective of the performer ("Down Left", "Up Right", etc).

### 5. Prepositions of place Worn and Held 
The preposition of place Worn can only described a position of a costume relative to an actor.
The preposition of place Held can only described a position of a costume or a prop relative to an actor.

| Item    | Position | Relative Position | Signification                               |
|---------|----------|-------------------|---------------------------------------------|
| Costume | Actor    | Worn              | This actor wears this costume               |
| Costume | Actor    | Held              | This actor holds the costume with his hands |
| Prop    | Actor    | Held              | This actor holds the prop with his hands    |

