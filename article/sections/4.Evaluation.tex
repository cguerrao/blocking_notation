
\section{Recommendations}

We used Excel as a prototyping tool, as it makes it easy to verify grammatical correctness but it is not an optimal way of writing notations. We identify some issues related to the ergonomics of a notation interface : taking in account the 5 categories, the 29 actions, the 3 or 4 indications to describe an action, how can we properly notate many actors and visualize the result ? How can we easily notate very long videos ? 

It is important when annotating to visualize every protagonist at the same time. However it is not necessarily to see the equivalent of our 16 columns in our Excel file for every actors all the time. The visualization used \autoref{fig:gui_view} is a good approach to notate a theater play with many actors.

In our opinion, it is better for a writer to use either its mouse or its keyboard to notate plays. Annotating can indeed be very long, so it is more comfortable to not switch between those two elements and helps us to gain a bit of time. To accelerate the notation process, auto-completion and prefilled notations are the best allies.

To help the writer, it is also good to check his notation and alerts him when he might write something wrong. The next situations should be avoided :  
\begin{itemize}
\item The writer notates the start of a new action whereas the last action from the same category is neither suspended nor finished.
\item The writer suspends or resumes an action which never was started or resumes an action which never was suspended.
\item The writer makes spelling mistakes or grammar. mistakes\footnote{A grammar mistake is defined here as an action described with wrong parameters. For example "an actor wears a chair" is a grammar mistake, because a chair is a "Prop" item and "Wear" is only followed with "Costume" items }.
\end{itemize}

\autoref{fig:gui_view} shows a proof of concept of a tool for visualizing notations. This interface is made of a video view and a sketch view. However this interface could be turn in an interface for notating. It has indeed a lot of advantages: 
\begin{itemize}
\item we see every actors at the same time
\item we can directly select items and move them which is more precise than with location indications (Up Left, Down Right...)
\item the interface can easily check correctness based on the Action State Machine
\end{itemize}
With this interface, we only would have to select an actor by clicking on his icon, then select an action from a menu and finally click on another actor/object/location (according to the \autoref{fig:moveAction} to \autoref{fig:grimaceAction}) in the sketch view to select the parameter. It will be better to see for each category the current performed action for the chosen actor, and by clicking on this action, we could change its state (suspend, resume, success, fail). Such interface guarantees the correctness of notations and considerably helps to ensure that all important actions are correctly annotated. Otherwise the sketch view will not match with the video view.

\section{Limitations and future work}

For our proof of concept, we arbitrary chose to annotate two different versions of the first scene of \textit{L'île des esclaves}. The next steps will be to test our system with other versions of \textit{L'île des esclaves}, other plays and with different writers. However these two recordings already highlighted some issues. 

Some notations can be ambiguous. Indeed: 

\textbf{1. An action's description depends on the writer's \textit{precision}:}

For example here are two equivalent descriptions. The first one is very precise whereas the second is not :

\begin{table}
    \centering
    \begin{tabular}{llr}
        \toprule 
        \multicolumn{3}{c}{Precise Notation} \\
        \cmidrule(r){1-3}
        State & Action & Attribute \\
        \midrule
        Start & TakeOff & Right Shoe \\
        Success & TakeOff & Right Shoe \\
        Start & TakeOff & Left Shoe \\
        Success & TakeOff & Left Shoe \\
        Start & TakeOff & Left Socket \\
        Success & TakeOff & Left Socket \\
        Start & TakeOff & Right Socket \\
        Success & TakeOff & Left Socket \\
    \end{tabular}

    \begin{tabular}{llr}
        \toprule
        \multicolumn{3}{c}{Unprecise Notation} \\
        \cmidrule(r){1-3}
        State & Action & Attribute \\
        \midrule
        Start & TakeOff & Shoes ; Sockets \\
        Success & TakeOff & Shoes ; Sockets \\
    \end{tabular}
    
    \caption{Equivalent Notation}
\end{table}


\textbf{2. The video quality impacts the description}

The video quality has a role in an action's description. Indeed, if the video has a too poor sound quality, we cannot detect the different intensity of a "Say" action (i.e. between "Whisper", "Talk", "Shout" etc.). 

If the image is also bad quality, it can be hard to distinguish the difference between two different locations (for example between "Down Left" and "Middle Left"). We recommend to use \textbf{a unique high angle shot} (and not an eye level shot). In fact, the high angle shot helps to easily determine the depth. It is also better to have a unique large shot, because some information can be lost with an edited video. A large shot and then a close-up shot on one protagonist are for example shown : we have to guess what the other actors are doing during this last shot.

\textbf{3. The \textit{stage division in nine blocks} can be ambiguous}

In the \textit{Blocking Notation} model, the stage is divided in nine blocks (defined with combinations of the words Up, Middle, Down, Left, Right and Center), but this representation is a fuzzy, not particularly accurate representation. What should be written if an actor or an object is located on the limit between two blocks ?

To have a more precise location, relative descriptions such as "Left to Arlequin" are allowed. But sometimes relative location cannot be used, in particular during the description of the first element. To correct this problem, the stage could be seen as a map with X and Y coordinates instead of nine blocks. But it is very hard (even impossible) for a human being to easily find those coordinates. They have to be calculated by a machine (with video analysis).

\textbf{5. Only 29 actions can be too rudimentary: }

To recreate a whole play in 3D-animation, we might need more than 29 actions. The play will probably be very redundant and will loose some precision. The 3D-animation generator has to add some noise, if we don't want to see the play as an automaton. As a matter of fact, one beauty of a play is its living and transient aspect.