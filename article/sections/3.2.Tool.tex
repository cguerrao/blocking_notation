\subsection{Blocking Notation Tools}

    \subsubsection{Template}

To see if our model can properly represent a theater play, we annotated play videos. These videos show interpretations of the play \textit{L'île des esclaves} by Marivaux in different languages. The videos were found from the project \textit{KinoAI}[10].

To write our \textit{Blocking Notations}, we created an Excel template with eight different sheets:
\begin{itemize}
\item Actor
\item Costume
\item Prop
\item Set
\item Stagecraft
\item Step by Step
\item Statistics
\item Vocabulary
\end{itemize}

We used the first five sheets to describe the different stage elements (Description phase) and how they appear at the beginning of the scene (Initialization phase). In the Vocabulary sheet, we defined the words that our ontology accepts. For example, we can find every allowed actions, their category and their possible states. We added Excel formulas in the whole file to check if the correct words were used. 

\begin{figure}[ht]
\centering
\includegraphics[scale=0.27]{excel_view.png}
\caption{View of the Step by Step sheet}
\label{fig:excel_view}
\end{figure}

The most important sheet is the Step by Step sheet which contains every \textit{Blocking Notation} (\autoref{fig:excel_view}). This sheet is a 2D chart : the horizontal axis displays all actors and their actions and the vertical axis represents the time reference (2 seconds per line). Each actor have 16 columns :
\begin{itemize}
\item 4 columns to notate the Touch actions
\item 3 columns for each other category of actions
\end{itemize}

\begin{figure}[ht]
\centering
\includegraphics[scale=0.24]{excel interpretation.png}
\caption{An example of \textit{Blocking Notations}' interpretation}
\label{fig:excel_interpretation}
\end{figure}

Each category has one column to write the action's name (for example "Walk") and one column to write the action's state (cf \autoref{fig:actions_state}). For each category, we used the last columns  to write the different action's parameters (according to the \autoref{fig:moveAction} to \autoref{fig:grimaceAction}). If several parameters are needed, they should be defined as a group i.e. every parameter are written in the same cell and are separated with a semicolon without any extra space.

For our proof of concept, we decided to have a fixed time reference : each row represent two seconds of the video. But in the future, we should have an abstract time reference. We chose to have 2 seconds per notations, because we estimate that a discrete action lasts about 2 seconds.
If a continuous action happens very fast (i.e. in 2 seconds or less), only the notation to indicate the end of the action should be written.

The last Statistics sheet gives a simple overview of the play. We can see the number of: 
\begin{itemize}
\item actors
\item costumes
\item props
\item set elements
\item stagecraft elements
\item performed actions per category and per actor
\item written notations
\end{itemize}

With this sheet, we can estimate if the \textit{Blocking Notation} system requires a lot of notations or not. We can also compare different staging and to see which actor is more active during a scene (cf \autoref{fig:stat}).

The French version by Irina is about 5 minutes long whereas the Spanish version by Isla is 8 minutes long. To annotate a play, we first tried to notate every actors at the same time, but moving from one actor to the other was complicated and slowed down the whole process. Then we tried to:
\begin{itemize}
\item focus on one actor and write all his notations
\item repeat the same process for each actor
\end{itemize}
So the video has to be viewed as many times as there are actors. In our opinion, this process is a bit quicker and reduces the notations mistakes, because our concentration is on one element and not on many elements. We needed one hour per actor per minute of video to notate. In comparison with the Benesh Notation, Dany Lévêque\footnote{choreologist and assistant to Angelin Preljocaj, who has transposed more than thirty choreographies and reassembled many works at the Center Chorégraphique National d'Aix en Provence or for other companies} needs around 8 to 12 hours to notate one minute of a dance video[2].

Even if notating with the \textit{Blocking Notation} system is a bit shorter than usual notations systems, it is still time-consuming. Moving between the video player and the Excel file slows down the annotation process. In future work we would like to provide a graphic interface integrated with the video player (as in Advene) allowing to annotate more easily states and actions (cf the following sections).


\subsubsection{Interface}

\begin{figure}[ht]
\centering
\includegraphics[scale=0.165]{gui_view.png}
\caption{Interface to visualize}
\label{fig:gui_view}
\end{figure}

To visualize in a better way our notation, we implemented an interface in Python. In this interface (\autoref{fig:gui_view}) we can see on the left side the video and on the right side a sketch representing the stage with a bird's-eye view. Actors are shown with icons, whereas objects are represented with rectangles. When we launch this interface, we see the actors moving on the sketch and on the video. That way we could efficiently compare our notations and the real video.

As it is only a proof concept, the interface only shows the actors movement (i.e. every Move Action). We leave for future work the implementation of other action's categories. This interface helped us to check our notations, by comparing our result and the real video. 