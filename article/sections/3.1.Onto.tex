\section{Methods}

The purpose of the \textit{Blocking Notation} project is to develop a formal representation of a theater play.

This representation relies on :
\begin{itemize}
\item an ontology to describe theater performances 
\item a graphical representation of the play
\end{itemize}

\subsection{Blocking Notation Ontology}

The \textit{Blocking Notation} ontology includes:
\begin{itemize}
\item the actors identified by their roles
\item actions performed on stage during a play
\item set elements, costumes, props
\item technical elements performed during the play (such as light, video, sound, etc.).
\end{itemize}

An action has to be performed by an actor during a specific time. We made a difference between discrete and continuous action. A discrete action will automatically be performed and will either success or fail. On the other hand, a continuous action has a state which evolve during time (cf \autoref{fig:actions_state}). A continuous action starts once and stops once and can, in between, be suspended and resumed as many times as we want. When a continuous action stops, it can be either a success or a fail.

\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{actions_state_diagram.png}
\caption{State Diagram of a continuous action}
\label{fig:actions_state}
\end{figure}

Each action has one category which can be:
\begin{enumerate}
\item \textbf{Move: } Action which involves the movement of an actor or a group of actors to a specific location on stage.
\item \textbf{Say: } Action which involves the use of an actor's voice (or of a group of actors).
\item \textbf{Touch: } Action which involves the contact of an actor or a group of actors with an item present on stage. The item must be close to the actor or the group of actors otherwise a Move Action  to get closer to the item must be performed before touching it.
\item \textbf{Gesture: } Action which involves the gesture of an actor or a group of actors which is neither a movement, nor an interaction with an object.
\item \textbf{Grimace: } Action which involves the use of facial expressions of an actor or a group of actors (for example to make faces).
\end{enumerate}

Two actions from the same category cannot be performed at the same time: an actor cannot run (Category Move) and walk (Category Move) at the same time whereas he can walk and talk (Category Say) at the same time. This property is mostly guaranteed for physiological reasons. However it could be possible to do two Touch Actions at the same time as an actor has two arms. But as it doesn't frequently happen and for integrity reasons, we chose to restrict one Touch Action at a time per actor.

29 different actions to describe the actions made by an actor during a play were defined.(cf \autoref{fig:moveAction} to \autoref{fig:grimaceAction})

\begin{figure}[ht]
\centering
\includegraphics[scale=0.38]{moveAction.png}
\caption{Relations between Move Actions and\textit{Blocking Notation} items}
\label{fig:moveAction}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[scale=0.6]{sayAction.png}
\caption{Relations between Say Actions and \textit{Blocking Notation} items}
\label{fig:sayAction}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[scale=0.38]{touchAction.png}
\caption{Relations between Touch Actions and \textit{Blocking Notation} items}
\label{fig:touchAction}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[scale=0.6]{gestureAction.png}
\caption{Relations between Gesture Actions and \textit{Blocking Notation} items}
\label{fig:gestureAction}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[scale=0.6]{grimaceAction.png}
\caption{Relations between Grimace Actions and \textit{Blocking Notation} items}
\label{fig:grimaceAction}
\end{figure}

The action "LookAt" and "Pose" are a bit particular, because an actor continually looks at somewhere and poses. In order to not overburden the notations, every updates on an actor's gaze or posture shouldn't be marked, but only those which are deemed necessary. Some actor's gaze or posture indication can be recreated following a common set of rules described by Christine Talbot and G. Michael Youngblood[1]: an actor looks (or changes his posture according to) the direction where he walks, who he is talking with, where he points... 

A location can either be a general location (i.e. a global position on stage), or a relative location (i.e a position defined by another actor, a costume, a prop or a set element). We decided to divide the stage into nine blocks. If a stage object or an actor is located on a border between two blocks, the block which is the closest to the audience and the most in the center, should be written. To describe a object's or an actor's position with a relative location, prepositions of place such as "Left To" and "Right To" can be used. These orientations are marked with the audience's point of view (and not from the actor's). But all nine locations on stage are from the perspective of the performer ("Down Left", "Up Right", etc).
